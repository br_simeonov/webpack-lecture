module.exports = {
	env: {
		browser: true,
		es6: true,
		node: true
	},
	extends: ['eslint:recommended', 'plugin:react/recommended'],
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly'
	},
	parser: 'babel-eslint',
	parserOptions: {
		ecmaVersion: 2018,
		ecmaFeatures: {
			jsx: true
		},
		sourceType: 'module'
	},
	plugins: ['react', 'prettier'],
	rules: {
		'no-console': ['warn'],
		'prettier/prettier': 'error'
	}
};
