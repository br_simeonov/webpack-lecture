module.exports = {
	printWidth: 120,
	useTabs: true,
	tabWidth: 4,
	semi: true,
	singleQuote: true,
	trailingComma: 'none',
	quoteProps: 'as-needed',
	jsxBracketSameLine: true,
	jsxSingleQuote: false
};
