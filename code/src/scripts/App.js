import React from 'react';
import { render } from 'react-dom';

import $ from 'jquery';
import 'bootstrap';

import 'bootstrap/scss/bootstrap.scss';
import '../styles/styles.scss';
import webpackLogo from '../images/webpack-logo.png';

import Accordion from './components/Accordion';
import Dropdown from './components/Dropdown';
import Popover from './components/Popover';
import Tooltip from './components/Tooltip';

class App extends React.Component {
	componentDidMount() {
		$('[data-toggle="popover"]').popover();
		$('[data-toggle="tooltip"]').tooltip();
	}

	render() {
		return (
			<div className="container">
				<p>
					<img src={webpackLogo} alt="webpack" width="50" /> Hello React App!!!
				</p>
				<div className="alert alert-info">Bootstrap danger!</div>
				<Dropdown />
				<Popover />
				<Tooltip />
				<Accordion />
			</div>
		);
	}
}

render(<App />, document.getElementById('app'));
