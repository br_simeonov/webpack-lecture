import React from 'react';

export default class Popover extends React.Component {
	render() {
		return (
			<button
				type="button"
				className="btn btn-danger mt-3"
				data-container="body"
				data-toggle="popover"
				data-placement="top"
				data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
				Popover on top
			</button>
		);
	}
}
