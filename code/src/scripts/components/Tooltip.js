import React from 'react';

export default class Tooltip extends React.Component {
	render() {
		return (
			<button
				type="button"
				className="btn btn-secondary ml-3 mt-3"
				data-toggle="tooltip"
				data-placement="top"
				title="Tooltip on top">
				Tooltip on Top
			</button>
		);
	}
}
