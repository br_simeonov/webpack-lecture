const webpack = require('webpack');
const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const BUILD_DIR = path.join(__dirname, 'dist');
const APP_DIR = path.join(__dirname, 'src');

const devEnv = process.env.NODE_ENV || 'development';

const config = {
	entry: {
		bundle: APP_DIR + '/scripts/App.js'
	},
	output: {
		path: BUILD_DIR,
		filename: 'scripts/[name].[hash].js'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: 'babel-loader'
			},
			{
				test: /\.(scss)$/,
				use: [
					{
						// loader: process.env.NODE_ENV !== "production" ? "style-loader" : MiniCssExtractPlugin.loader, // inject CSS to page
						// loader: MiniCssExtractPlugin.loader, // inject CSS to page
						loader: MiniCssExtractPlugin.loader,
						options: {
							hmr: devEnv === 'development',
							reloadAll: true
						}
					},
					{
						loader: 'css-loader', // translates CSS into CommonJS modules
						options: {
							sourceMap: true
						}
					},
					{
						loader: 'postcss-loader', // Run post css actions
						options: {
							sourceMap: true,
							plugins: function() {
								// post css plugins, can be exported to postcss.config.js
								return [require('precss'), require('autoprefixer')];
							}
						}
					},
					{
						loader: 'sass-loader', // compiles Sass to CSS
						options: {
							sourceMap: true
						}
					}
				]
			},
			{
				test: /\.png?/,
				include: APP_DIR,
				use: {
					loader: 'file-loader',
					options: {
						outputPath: 'images'
					}
				}
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: 'styles/[name].css',
			chunkFilename: 'styles/[name].css'
		}),
		new HtmlPlugin({
			title: 'My Generated Webpack Index',
			template: APP_DIR + '/index.html',
			filename: 'index.html'
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	optimization: {
		splitChunks: {
			cacheGroups: {
				vendor: {
					test: /node_modules/,
					chunks: 'initial',
					name: 'vendor',
					enforce: true
				}
			}
		}
	},
	devServer: {
		port: 1122,
		contentBase: 'dist',
		compress: true,
		open: true,
		hot: true,
		disableHostCheck: false,
		watchContentBase: true // watch for html files into contentBase
	},
	devtool: 'source-map'
};

module.exports = config;
